<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Dropfile;
use App\Models\Imageitem;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->dropbox = Storage::disk('dropbox')->getDriver()->getAdapter()->getClient();
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            $data = Item::all();
//        dd($data);
            return DataTables::of($data)->addColumn('customer', function ($data) {
                $customers = $data->customer->name;
                return $customers;
            })->addColumn('category', function ($data) {
                $categorys = $data->category->name;
                return $categorys;
            })->addColumn('action', function ($data) {
                $button = '<a href="' . route('admin.item.show', $data->id) . '" class="btn btn-info"><i class="fa fa fa-eye"></i></a>';
                $button .= '<a href="item/'.$data->id.'/edit'.'" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>';
                $button .= '<a  href="'.route('admin.itemDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                return $button;
            })->rawColumns(['customer','category','action'])->make(true);
        }
        return view('admin.content.item.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeImg(Request $request,$id){
//        $path =$request->file('image')->store('public');
//        $path =Storage::putFile('public',$request->file('image'));
//        $path =$request->file('image')->storeAs('public','gambar');
        if ($request->hasFile('image'))
        {
            $files = $request->file('image');
            foreach ($files as $file){
                $data = new Imageitem();

                $name = rand(99999,1);
                $extension = $file->getClientOriginalExtension();
                $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
                Storage::putFileAs('public',$file,$newName);

                $imgDB = 'storage/'.$newName;
                $data->item_id = $id;
                $data->img = $imgDB;
                $data->dsc = $imgDB;

                $data->save();
            }

        }
        return redirect()->back()->with('success','success upload image');
     }
    public function showImg(){

    }
    public function create()
    {
        $categories = Category::all();
        $customers = Customer::all();
        view()->share([
            'categories' => $categories,
            'customers' => $customers
        ]);
        return view('admin.content.item.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Item();

        $validates = [
            'customer_id' => 'required',
            'category_id' => 'required',
            'name' => 'required',
            'color' => 'required',
            'size' => 'required',
            'price' => 'required|numeric',
            'dsc' => 'required',
        ];
        $request->validate($validates);
//        dd($data);

        $data->customer_id = $request->customer_id;
        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->color = $request->color;
        $data->size = $request->size;
        $data->price = $request->price;
        $data->dsc = $request->dsc;

        $data->save();
        return redirect()->route('admin.item.index')->with('success','add product success');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Item::find($id);
        $img = Imageitem::where('item_id',$id)->get();
        view()->share([
            'data' => $data,
            'image' => $img,
        ]);
        return view('admin.content.item.show');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Item::find($id);
        $img = Imageitem::where('item_id',$id)->get();
        $categories = Category::all();
        $customers = Customer::all();

        view()->share([
            'data' => $data,
            'categories' => $categories,
            'customers' => $customers,
            'image' => $img,

        ]);
        return view('admin.content.item.edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Item::findOrFail($id);

        $validates = [
            'customer_id' => 'required',
            'category_id' => 'required',
            'name' => 'required',
            'color' => 'required',
            'size' => 'required',
            'price' => 'required|numeric',
            'dsc' => 'required',
        ];
        $request->validate($validates);
//        dd($data);

        $data->customer_id = $request->customer_id;
        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->color = $request->color;
        $data->size = $request->size;
        $data->price = $request->price;
        $data->dsc = $request->dsc;

        $data->save();
        return redirect()->route('admin.item.index')->with('warning','edit product success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Item::findOrFail($id);
        $images = Imageitem::where('item_id',$id)->count();
        for ($i=1;$i<=$images;$i++){
            $img = Imageitem::where('item_id',$id)->first();
            $img->delete();
        }

        $data->delete();

        return redirect()->route('admin.item.index')->with('danger','delete product success');

    }
}
