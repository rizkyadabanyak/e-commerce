<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class View
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Session::has('customer')){
            return redirect()->route('customer.index');
        }
        return $next($request);
    }
}
