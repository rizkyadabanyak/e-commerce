<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ecommerce Dashboard &mdash; Stisla</title>
@include('admin.component.style')

<body>
<div id="app">
    <div class="main-wrapper">

        @include('admin.partials.nav')

        @include('admin.partials.sidebar')

        <div class="wrapper">
            @yield('content')
        </div>
        @include('admin.partials.footer')

    </div>
</div>

@include('admin.component.footer-script')
</body>
</html>
