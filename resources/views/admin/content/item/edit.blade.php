@extends('admin.layout')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Product</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Item</h4>

                                    </div>
                                </div>
                            </div>
                                <div class="card-body">
                                    <form action="{{route('admin.item.update',$data->id)}}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Product Name</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="text" name="name" id="name" class="form-control" value="{{$data->name}}">
                                                @error('name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Product Customer</label>
                                            <div class="col-sm-12 col-md-7">
                                                <select class="form-control select2" name="customer_id">
                                                    @if($data->customer_id != null)
                                                        <option value="{{$data->customer_id}}">{{$data->customer->name}}</option>
                                                    @endif
                                                    @foreach($customers as $customer)
                                                        @if($customer->id != $data->customer_id)
                                                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                                                            @endif
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Product Category</label>
                                            <div class="col-sm-12 col-md-7">
                                                <select class="form-control select2" name="category_id">
                                                    @if($data->category_id != null)
                                                        <option value="{{$data->category_id}}">{{$data->category->name}}</option>
                                                    @endif
                                                    @foreach($categories as $category)
                                                        @if($category->id != $data->category_id)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Product Color</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="text" name="color" id="color" class="form-control" value="{{$data->color}}">
                                                @error('color')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Product Size</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="text" name="size" id="size" class="form-control" value="{{$data->size}}">
                                                @error('size')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Price</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="text" name="price" id="price" class="form-control" value="{{$data->price}}">
                                                @error('price')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description</label>
                                            <div class="col-sm-12 col-md-7">
                                                <input type="text" name="dsc" id="dsc" class="form-control" value="{{$data->dsc}}">
                                                @error('dsc')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                            <div class="col-sm-12 col-md-7">
                                                <button class="btn btn-primary">Publish</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Image</h4>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{route('admin.storeImg',$data->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Product Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image[]" id="" multiple="true">
                                        </div>

                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Publish</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="gallery gallery-md">
                                    @foreach($image as $img)
                                        <div class="gallery-item" data-image="{{asset($img->img)}}" data-title="{{$img->img}}"></div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection
