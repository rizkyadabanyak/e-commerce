<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ecommerce Dashboard &mdash; Stisla</title>
@include('customer.component.style')

<body>
<div id="app">
    <div class="main-wrapper">
        @include('customer.partials.nav')

        @include('customer.partials.sidebar')

        <div class="wrapper">
            @yield('content')
        </div>
        @include('customer.partials.footer')
    </div>
</div>
@include('customer.component.footer-script')

</body>
</html>
