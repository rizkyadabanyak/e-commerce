<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $customer = \Session::get('customer');

        view()->share([
            'customer' => $customer
        ]);

        return view('customer.content.home');
    }
}
