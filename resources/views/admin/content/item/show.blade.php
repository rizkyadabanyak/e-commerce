@extends('admin.layout')

@section('content')
    @include('admin.component.data-tabel.head-datatebel')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Product</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="container">

                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h2>Detail Item</h2>

                                    </div>
                                    <div>
                                    </div>
                                </div>

                            </div>

                            <div class="card-body">
                                <div class="container"><br>
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-6">
{{--                                            <div class="row">--}}
{{--                                                <div class="col-sm-6 col-lg-6">--}}
{{--                                                    <h5>Seller : <a>{{$data->customer->name}}</a></h5>--}}
{{--                                                    <h5>Name : <a>{{$data->name}}</a></h5>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-sm-6 col-lg-6">--}}
{{--                                                    dqwdqw--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <h5>Product Name : <a>{{$data->name}}</a></h5>
                                            <h5>Product Category : <a>{{$data->Category->name}}</a></h5>
                                            <h5>Seller : <a>{{$data->customer->name}}</a></h5>
                                            <h5>Product Size : <a>{{$data->size}}</a></h5>
                                            <h5>Product Color : <a>{{$data->color}}</a></h5>
                                            <h5>Product Price : <a>{{$data->price}}</a></h5>
                                            <h5>Product Description : <a>{{$data->dsc}}</a></h5>

                                        </div>
                                        <div class="col-sm-6 col-lg-6">
                                            <div class="gallery gallery-md">
                                                @foreach($image as $img)
                                                    <div class="gallery-item" data-image="{{asset($img->img)}}" data-title="{{$img->img}}"></div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
