<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    public function tmp(){
        return $this->hasMany(Tmpaddress::class);
    }
    public function item(){
        return $this->belongsToMany(Item::class);
    }
}
