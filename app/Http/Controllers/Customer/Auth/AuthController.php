<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function getLogin(){
        return view('customer.auth.login');
    }
    public function postLogin(Request $request){
        $customer = Customer::where('email',$request->email)->first();

        if ($customer == null){
            \Session::flash('error','email tidak di temukan');
            return redirect()->route('customer.auth.login');
        }
        if (\Hash::check($request->password,$customer->password)){
            \Session::put('customer',$customer);
            \Session::flash('msg','berhasil login');

            return redirect()->route('home');
        }else{
            \Session::flash('error','email tidak cocok ');

            return redirect()->route('customer.auth.login');
        }


        return 'ini fungsi login';
    }

    public function getRegis(){
        return view('customer.auth.regis');
    }
    public function postRegis(Request $request){

        $validate = [
            'name' => 'required',
            'email' => 'required|email|unique:customers',
            'password' => 'required|min:6|confirmed'
        ];
        $request->validate($validate);


        $data = new Customer();

        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->email = $request->email;
        $data->password = bcrypt($request->password);

        $data->save();

        return redirect()->back();

    }

    public function logout(){
        \Session::flush();

        return redirect()->route('customer.auth.login');
    }
}
