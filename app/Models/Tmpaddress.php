<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tmpaddress extends Model
{
    use HasFactory;

    public function customers(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function address(){
        return $this->belongsTo(Address::class);
    }
}
