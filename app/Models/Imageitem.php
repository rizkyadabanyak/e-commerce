<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imageitem extends Model
{
    use HasFactory;
    protected $table = 'image_items';

    public function item(){
        return $this->belongsTo(Item::class);
    }
}
