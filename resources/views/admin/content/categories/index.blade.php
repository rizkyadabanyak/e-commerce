@extends('admin.layout')

@section('content')
    @include('admin.component.data-tabel.head-datatebel')

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Product</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    <p>{{ session('success') }}</p>
                                </div>
                            @elseif(session('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ session('danger') }}</p>
                                </div>
                            @endif
                          <div class="container">

                            <div class="d-flex justify-content-between pt-5">
                                <div>
                                    <h4>Category Item</h4>

                                </div>
                                <div>
                                    <a href="{{route('admin.categories.create')}}" class="btn btn-success">New</a>

                                </div>
                            </div>

                          </div>
                            <br>


                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="category">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Dsc</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

        @include('admin.component.data-tabel.script-datatabel')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#category').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('admin.categories.index') }}",
                    },
                    columns: [
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'dsc',
                            name: 'dsc'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        },

                    ]
                });
            });
        </script>
@endsection
