<?php

namespace App\Http\Controllers\Customer\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $customer = \Session::get('customer');

        view()->share([
            'customer' => $customer
        ]);

        return view('customer.dashboard.index');
    }
}
