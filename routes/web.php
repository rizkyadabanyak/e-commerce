<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('',[\App\Http\Controllers\Customer\HomeController::class,'index'])->name('home');


Route::group(['prefix'=>'admin','as'=>'admin.'],function (){
    Route::middleware([])->group(function () {
        Route::get('dashboard',[\App\Http\Controllers\Admin\DashboardController::class, 'index'])->middleware(['web'])->name('dashboard');


        Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
        Route::get('categories/destroy/{id}', [\App\Http\Controllers\Admin\CategoryController::class,'destroy'])->name('categoriesDestroy');

        Route::resource('item', \App\Http\Controllers\Admin\ItemController::class);
        Route::get('item/destroy/{id}', [\App\Http\Controllers\Admin\ItemController::class,'destroy'])->name('itemDestroy');
        Route::post('item/img/{id}',[\App\Http\Controllers\Admin\ItemController::class,'storeImg'])->name('storeImg');
    });
}


);


Route::group(['prefix'=>'customer','as'=>'customer.'],function (){

    Route::group(['as'=>'auth.','middleware'=>['view']],function (){
        Route::get('login',[\App\Http\Controllers\Customer\Auth\AuthController::class,'getLogin'])->name('login');
        Route::post('postLogin',[\App\Http\Controllers\Customer\Auth\AuthController::class,'postLogin'])->name('postLogin');
        Route::get('regis',[\App\Http\Controllers\Customer\Auth\AuthController::class,'getRegis'])->name('regis');
        Route::post('postRegis',[\App\Http\Controllers\Customer\Auth\AuthController::class,'postRegis'])->name('postRegis');
    });

    Route::get('logout',[\App\Http\Controllers\Customer\Auth\AuthController::class,'logout'])->name('logout');


    Route::group(['prefix'=>'dashboard','as'=>'dashboard.','middleware'=>['customer']],function (){
        Route::get('/',[\App\Http\Controllers\Customer\Dashboard\DashboardController::class,'index'])->name('index');
    });
});
