<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
            <ul class="sidebar-menu">
            <li class="menu-header">Super Admin</li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-columns"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                    <li class="active"><a class="nav-link" href="index.html">Customer</a></li>
                    <li class="active"><a class="nav-link" href="index.html">Seller</a></li>
                </ul>
            </li>
            <li class="menu-header">Page</li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Product</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('admin.item.index')}}">Item</a></li>
                    <li><a class="nav-link" href="{{route('admin.categories.index')}}">Category Item</a></li>

                </ul>
            </li>
        </ul>

    </aside>
</div>
